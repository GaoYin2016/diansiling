package com.zkteam.drivertools.bean;

/**
 * 作者：${User}
 * 电话：18810474975
 * 邮箱：18810474975@163.com
 * 版本号：
 * 类描述：
 * 修改时间：${DATA}2058
 */

public class UpdataOrderInfo {

    /**
     * BaseResponse : {"Ret":-1,"Msg":"用户名或密码不能为空"}
     */
    private BaseResponseEntity BaseResponse;

    public BaseResponseEntity getBaseResponse() {
        return BaseResponse;
    }

    public void setBaseResponse(BaseResponseEntity BaseResponse) {
        this.BaseResponse = BaseResponse;
    }

    public static class BaseResponseEntity {
        /**
         * Ret : -1
         * Msg : 用户名或密码不能为空
         */

        private int Ret;
        private String Msg;

        public int getRet() {
            return Ret;
        }

        public void setRet(int Ret) {
            this.Ret = Ret;
        }

        public String getMsg() {
            return Msg;
        }

        public void setMsg(String Msg) {
            this.Msg = Msg;
        }
    }
}
