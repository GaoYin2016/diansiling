package com.zkteam.drivertools;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bigkoo.svprogresshud.SVProgressHUD;
import com.zkteam.drivertools.utils.SharedPreferenceUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * 作者：gaoyin
 * 电话：18810474975
 * 邮箱：18810474975@163.com
 * 版本号：1.0
 * 类描述：   订单信息
 * 备注消息：
 * 修改时间：2017/7/8 下午4:06
 **/
public class UserInfoActivity extends AppCompatActivity {
    protected Unbinder unbinder;
    @BindView(R.id.img_signed)
    ImageView imgSigned;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.img_msg)
    ImageView imgMsg;
    @BindView(R.id.tv_userinfo_name)
    TextView tvUserinfoName;
    @BindView(R.id.rl_userinfo)
    RelativeLayout rlUserinfo;
    @BindView(R.id.btn_unlogin)
    Button btnUnlogin;
    private SVProgressHUD svProgressHUD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userinfo);
        unbinder = ButterKnife.bind(this);
        svProgressHUD = new SVProgressHUD(this);
        tvTitle.setText("用户中心");
        imgMsg.setVisibility(View.GONE);
        imgSigned.setVisibility(View.VISIBLE);
        initData();
    }

    private void initData() {
        tvUserinfoName.setText(SharedPreferenceUtils.getStringData("username", ""));
    }

    @OnClick(R.id.btn_unlogin)
    public void unLogin() {
        SharedPreferenceUtils.remove("uid");
        startActivity(new Intent(this, LoginActivity.class));
        this.finish();
    }

    @OnClick(R.id.img_signed)
    public void back() {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (unbinder != null)
            unbinder.unbind();
    }
}
