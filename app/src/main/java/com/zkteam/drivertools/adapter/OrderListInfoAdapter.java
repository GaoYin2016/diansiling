package com.zkteam.drivertools.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zkteam.drivertools.OrderInfoActivity;
import com.zkteam.drivertools.R;
import com.zkteam.drivertools.bean.OrderInfoList;
import com.zkteam.drivertools.ui.refreshview.recyclerview.BaseRecyclerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者：${User}
 * 电话：18810474975
 * 邮箱：18810474975@163.com
 * 版本号：
 * 类描述：   订单信息列表
 * 修改时间：${DATA}1748
 */
public class OrderListInfoAdapter extends BaseRecyclerAdapter<RecyclerView.ViewHolder> {

    private List<OrderInfoList.OrderListEntity> mOrderListEntity;
    private Context context;


    public OrderListInfoAdapter(Context context) {
        this.context = context;
        this.mOrderListEntity = new ArrayList<OrderInfoList.OrderListEntity>();
    }

    public void getOrderInfo(List<OrderInfoList.OrderListEntity> mOrderListEntity) {
        this.mOrderListEntity.clear();
        this.mOrderListEntity.addAll(mOrderListEntity);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder getViewHolder(View view) {
        return new OrderInfoListHolder(view);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType, boolean isItem) {
        return new OrderInfoListHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_orderinfo, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position, boolean isItem) {

        if (holder instanceof OrderInfoListHolder) {
            bindFaceScoreHolder((OrderInfoListHolder) holder, position);
        }
    }

    private void bindFaceScoreHolder(OrderInfoListHolder holder, final int position) {
        if (mOrderListEntity.get(position).getState() == 1) {
            holder.btn_order_state.setBackgroundResource(R.drawable.order_state_01);
            holder.btn_order_state.setText("新订单");
        } else if (mOrderListEntity.get(position).getState() == 2) {
            holder.btn_order_state.setBackgroundResource(R.drawable.order_state_02);
            holder.btn_order_state.setText("进行中");
        } else if (mOrderListEntity.get(position).getState() == 3) {
            holder.btn_order_state.setBackgroundResource(R.drawable.order_state_03);
            holder.btn_order_state.setText("已完成");
        } else if (mOrderListEntity.get(position).getState() == 4) {
            holder.btn_order_state.setBackgroundResource(R.drawable.order_state_04);
            holder.btn_order_state.setText("超时单");
        }
        holder.tv_order_number.setText(mOrderListEntity.get(position).getOrder_no());
        holder.tv_order_name.setText(mOrderListEntity.get(position).getUsername());
        holder.tv_order_phone.setText(mOrderListEntity.get(position).getPhone());
        holder.tv_order_local.setText(mOrderListEntity.get(position).getGoal_position());
        holder.tv_order_time.setText(mOrderListEntity.get(position).getBegin_time());
        holder.rl_order_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, OrderInfoActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("OrderInfo", mOrderListEntity.get(position));
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getAdapterItemCount() {
        return mOrderListEntity.size();
    }

    public class OrderInfoListHolder extends RecyclerView.ViewHolder {

        public Button btn_order_state;
        public TextView tv_order_number;
        public TextView tv_order_name;
        public TextView tv_order_local;
        public TextView tv_order_time;
        public TextView tv_order_phone;
        public RelativeLayout rl_order_item;

        public OrderInfoListHolder(View view) {
            super(view);
            btn_order_state = (Button) view.findViewById(R.id.btn_order_state);
            tv_order_number = (TextView) view.findViewById(R.id.tv_order_number);
            tv_order_name = (TextView) view.findViewById(R.id.tv_order_name);
            tv_order_local = (TextView) view.findViewById(R.id.tv_order_local);
            tv_order_time = (TextView) view.findViewById(R.id.tv_order_time);
            tv_order_phone = (TextView) view.findViewById(R.id.tv_order_phone);
            rl_order_item = (RelativeLayout) view.findViewById(R.id.rl_order_item);

        }
    }
}
