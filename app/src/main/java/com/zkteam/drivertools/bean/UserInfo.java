package com.zkteam.drivertools.bean;

/**
 * 作者：${User}
 * 电话：18810474975
 * 邮箱：18810474975@163.com
 * 版本号：
 * 类描述：
 * 修改时间：${DATA}1531
 */

public class UserInfo {

    /**
     * BaseResponse : {"Ret":0,"Msg":"查询成功"}
     * UserList : {"uid":1,"name":"asdf","car_no":"甘F30966"}
     * token : BE16B82E700CEF7B198042160156BB16
     */

    private BaseResponseEntity BaseResponse;
    private UserListEntity UserList;
    private String token;

    public BaseResponseEntity getBaseResponse() {
        return BaseResponse;
    }

    public void setBaseResponse(BaseResponseEntity BaseResponse) {
        this.BaseResponse = BaseResponse;
    }

    public UserListEntity getUserList() {
        return UserList;
    }

    public void setUserList(UserListEntity UserList) {
        this.UserList = UserList;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public static class BaseResponseEntity {
        /**
         * Ret : 0
         * Msg : 查询成功
         */

        private int Ret;
        private String Msg;

        public int getRet() {
            return Ret;
        }

        public void setRet(int Ret) {
            this.Ret = Ret;
        }

        public String getMsg() {
            return Msg;
        }

        public void setMsg(String Msg) {
            this.Msg = Msg;
        }
    }

    public static class UserListEntity {
        /**
         * uid : 1
         * name : asdf
         * car_no : 甘F30966
         */

        private int uid;
        private String name;
        private String car_no;

        public int getUid() {
            return uid;
        }

        public void setUid(int uid) {
            this.uid = uid;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCar_no() {
            return car_no;
        }

        public void setCar_no(String car_no) {
            this.car_no = car_no;
        }
    }

    @Override
    public String toString() {
        return "{" +
                "BaseResponse:" + BaseResponse +
                ", UserList:" + UserList +
                ", token:'" + token + '\'' +
                '}';
    }
}
