package com.zkteam.drivertools.bean;

import java.io.Serializable;
import java.util.List;

/**
 * 作者：${User}
 * 电话：18810474975
 * 邮箱：18810474975@163.com
 * 版本号：
 * 类描述：
 * 修改时间：${DATA}1644
 */

public class OrderInfoList {

    /**
     * BaseResponse : {"Ret":0,"Msg":"查询成功"}
     * OrderList : [{"state":3,"order_no":"5120170418173819","go_position":"沙州镇","goal_position":"莫高镇","begin_time":"2017-08-19 08:37:30","username":"王璐","phone":"13629373111","car_no":"甘F30966","start_mileage":"13.5","end_mileage":"15.5"},{"state":4,"order_no":"120170609093213","go_position":"测试","goal_position":"测试","begin_time":"2017-06-09 09:31:56","username":"管理员","phone":"15309423596","car_no":"甘F30966","start_mileage":null,"end_mileage":null}]
     * token : 0F106B9B21DA07A117947281658B8EFF
     */

    private BaseResponseEntity BaseResponse;
    private String token;
    private List<OrderListEntity> OrderList;

    public BaseResponseEntity getBaseResponse() {
        return BaseResponse;
    }

    public void setBaseResponse(BaseResponseEntity BaseResponse) {
        this.BaseResponse = BaseResponse;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<OrderListEntity> getOrderList() {
        return OrderList;
    }

    public void setOrderList(List<OrderListEntity> OrderList) {
        this.OrderList = OrderList;
    }

    public static class BaseResponseEntity {
        /**
         * Ret : 0
         * Msg : 查询成功
         */

        private int Ret;
        private String Msg;

        public int getRet() {
            return Ret;
        }

        public void setRet(int Ret) {
            this.Ret = Ret;
        }

        public String getMsg() {
            return Msg;
        }

        public void setMsg(String Msg) {
            this.Msg = Msg;
        }
    }

    public static class OrderListEntity implements Serializable {
        /**
         * state : 3
         * order_no : 5120170418173819
         * go_position : 沙州镇
         * goal_position : 莫高镇
         * begin_time : 2017-08-19 08:37:30
         * username : 王璐
         * phone : 13629373111
         * car_no : 甘F30966
         * start_mileage : 13.5
         * end_mileage : 15.5
         */

        private int state;
        private String order_no;
        private String go_position;
        private String goal_position;
        private String begin_time;
        private String username;
        private String phone;
        private String car_no;
        private String start_mileage;
        private String end_mileage;

        public int getState() {
            return state;
        }

        public void setState(int state) {
            this.state = state;
        }

        public String getOrder_no() {
            return order_no;
        }

        public void setOrder_no(String order_no) {
            this.order_no = order_no;
        }

        public String getGo_position() {
            return go_position;
        }

        public void setGo_position(String go_position) {
            this.go_position = go_position;
        }

        public String getGoal_position() {
            return goal_position;
        }

        public void setGoal_position(String goal_position) {
            this.goal_position = goal_position;
        }

        public String getBegin_time() {
            return begin_time;
        }

        public void setBegin_time(String begin_time) {
            this.begin_time = begin_time;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getCar_no() {
            return car_no;
        }

        public void setCar_no(String car_no) {
            this.car_no = car_no;
        }

        public String getStart_mileage() {
            return start_mileage;
        }

        public void setStart_mileage(String start_mileage) {
            this.start_mileage = start_mileage;
        }

        public String getEnd_mileage() {
            return end_mileage;
        }

        public void setEnd_mileage(String end_mileage) {
            this.end_mileage = end_mileage;
        }
    }

    @Override
    public String toString() {
        return "{" +
                "BaseResponse:" + BaseResponse +
                ", token:'" + token + '\'' +
                ", OrderList:" + OrderList +
                '}';
    }
}
