package com.zkteam.drivertools;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bigkoo.svprogresshud.SVProgressHUD;
import com.google.gson.Gson;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.adapter.Call;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.convert.StringConvert;
import com.lzy.okgo.model.Response;
import com.zkteam.drivertools.api.NetWorkApi;
import com.zkteam.drivertools.bean.OrderInfoList;
import com.zkteam.drivertools.bean.UpdataOrderInfo;
import com.zkteam.drivertools.utils.SharedPreferenceUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * 作者：gaoyin
 * 电话：18810474975
 * 邮箱：18810474975@163.com
 * 版本号：1.0
 * 类描述：   订单信息
 * 备注消息：
 * 修改时间：2017/7/8 下午4:06
 **/
public class OrderInfoActivity extends AppCompatActivity {
    protected Unbinder unbinder;
    @BindView(R.id.img_signed)
    ImageView imgSigned;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.img_msg)
    ImageView imgMsg;
    @BindView(R.id.tv_order_local)
    TextView tvOrderLocal;
    @BindView(R.id.rl_orderinfo_local)
    RelativeLayout rlOrderinfoLocal;
    @BindView(R.id.tv_order_number)
    TextView tvOrderNumber;
    @BindView(R.id.rl_orderinfo_number)
    RelativeLayout rlOrderinfoNumber;
    @BindView(R.id.tv_order_time)
    TextView tvOrderTime;
    @BindView(R.id.rl_orderinfo_time)
    RelativeLayout rlOrderinfoTime;
    @BindView(R.id.tv_order_name)
    TextView tvOrderName;
    @BindView(R.id.rl_orderinfo_name)
    RelativeLayout rlOrderinfoName;
    @BindView(R.id.tv_order_phone)
    TextView tvOrderPhone;
    @BindView(R.id.rl_orderinfo_phone)
    RelativeLayout rlOrderinfoPhone;
    @BindView(R.id.tv_star_local)
    TextView tvStarLocal;
    @BindView(R.id.et_start_local)
    EditText etStartLocal;
    @BindView(R.id.tv_end_local)
    TextView tvEndLocal;
    @BindView(R.id.et_end_local)
    EditText etEndLocal;
    @BindView(R.id.btn_start)
    Button btnStart;
    @BindView(R.id.btn_end)
    Button btnEnd;
    private SVProgressHUD svProgressHUD;
    private OrderInfoList.OrderListEntity mOrderInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orderinfo);
        unbinder = ButterKnife.bind(this);
        tvTitle.setText("订单详情");
        imgSigned.setVisibility(View.VISIBLE);
        imgMsg.setImageResource(R.drawable.message);
        svProgressHUD = new SVProgressHUD(this);
        initData();

    }

    private void initData() {
        mOrderInfo = (OrderInfoList.OrderListEntity) getIntent().getExtras().getSerializable("OrderInfo");
        if (mOrderInfo != null) {
            tvOrderLocal.setText(mOrderInfo.getGoal_position() + "");
            tvOrderNumber.setText(mOrderInfo.getOrder_no() + "");
            tvOrderTime.setText(mOrderInfo.getBegin_time() + "");
            tvOrderName.setText(mOrderInfo.getUsername() + "");
            tvOrderPhone.setText(mOrderInfo.getPhone() + "");
        } else {
            svProgressHUD.showErrorWithStatus("获取订单详情失败！");
        }
    }

    /**
     * 开始 发车
     */
    @OnClick(R.id.btn_start)
    public void startCar() {
        if (etStartLocal.getText().toString().equals("")) {
            svProgressHUD.showInfoWithStatus("输入公里数！");
        } else {
            if (Integer.valueOf(etStartLocal.getText().toString()).intValue() >= 0) {
                CarExecute(etStartLocal.getText().toString(), 1);
            } else {
                svProgressHUD.showInfoWithStatus("请输入大于零整数");
            }

        }
    }

    /**
     * 录入  开始数据
     */
    private void CarExecute(final String Number, final int Type) {
        svProgressHUD.showWithStatus("正在录入中...");
        new Thread() {
            @Override
            public void run() {

                String uid = SharedPreferenceUtils.getStringData("uid", "");
                String token = SharedPreferenceUtils.getStringData("token", "");
                if (Type == 1) {
                    Call<String> call = OkGo.<String>get(NetWorkApi.getSartCarApi)
                            .params("uid", uid)
                            .params("token", token)
                            .params("order_no", mOrderInfo.getOrder_no())
                            .params("metre", Number)
                            .converter(new StringConvert())
                            .adapt();
                    call.execute(new StringCallback() {
                        @Override
                        public void onSuccess(final Response<String> response) {

//                        loginSuccess(userInfo);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    svProgressHUD.dismissImmediately();
                                    UpdataOrderInfo mUpdataOrderInfo = new Gson().fromJson(response.body().toString(), UpdataOrderInfo.class);
                                    if (mUpdataOrderInfo.getBaseResponse().getRet() == 0) {
                                        svProgressHUD.showSuccessWithStatus("录入成功！");
                                    } else {
                                        svProgressHUD.showErrorWithStatus("录入失败！");
                                    }
                                }
                            });
                            Log.e("ok", response.body().toString());
                        }

                        /**
                         * 服务器错误
                         *
                         * @param response
                         */
                        @Override
                        public void onError(Response<String> response) {
                            Log.e("onError", response.body().toString());
                            super.onError(response);
                        }
                    });
                } else if (Type == 2) {
                    Call<String> call = OkGo.<String>get(NetWorkApi.getEndCarApi)
                            .params("uid", uid)
                            .params("token", token)
                            .params("order_no", mOrderInfo.getOrder_no())
                            .params("metre", Number)
                            .converter(new StringConvert())
                            .adapt();
                    call.execute(new StringCallback() {
                        @Override
                        public void onSuccess(final Response<String> response) {
//                        loginSuccess(userInfo);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    svProgressHUD.dismissImmediately();
                                    UpdataOrderInfo mUpdataOrderInfo = new Gson().fromJson(response.body().toString(), UpdataOrderInfo.class);
                                    if (mUpdataOrderInfo.getBaseResponse().getRet() == 0) {
                                        svProgressHUD.showSuccessWithStatus("录入成功！");
                                    } else {
                                        svProgressHUD.showErrorWithStatus("录入失败！");
                                    }
                                }
                            });
                            Log.e("ok", response.body().toString());
                        }

                        /**
                         * 服务器错误
                         *
                         * @param response
                         */
                        @Override
                        public void onError(Response<String> response) {
                            Log.e("onError", response.body().toString());
                            super.onError(response);
                        }
                    });


                }
            }
        }.start();
    }

    /**
     * 结束 发车
     */
    @OnClick(R.id.btn_end)
    public void endCar() {
        if (etEndLocal.getText().toString().equals("")) {
            svProgressHUD.showInfoWithStatus("输入公里数！");
        } else {
            if (Integer.valueOf(etEndLocal.getText().toString()).intValue() >= 0) {
                CarExecute(etEndLocal.getText().toString(), 2);
            } else {
                svProgressHUD.showInfoWithStatus("请输入大于零整数");
            }
        }
    }

    @OnClick(R.id.img_signed)
    public void back() {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (unbinder != null)
            unbinder.unbind();
    }
}
