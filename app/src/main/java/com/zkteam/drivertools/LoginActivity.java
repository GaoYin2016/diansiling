package com.zkteam.drivertools;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.bigkoo.svprogresshud.SVProgressHUD;
import com.google.gson.Gson;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.adapter.Call;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.convert.StringConvert;
import com.lzy.okgo.model.Response;
import com.zkteam.drivertools.api.NetWorkApi;
import com.zkteam.drivertools.bean.UserInfo;
import com.zkteam.drivertools.utils.SharedPreferenceUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * 作者：${User}
 * 电话：18810474975
 * 邮箱：18810474975@163.com
 * 版本号：
 * 类描述：登录
 * 修改时间：${DATA}1312
 */
public class LoginActivity extends AppCompatActivity {
    protected Unbinder unbinder;
    @BindView(R.id.icon_me)
    ImageView iconMe;
    @BindView(R.id.et_loginTel)
    EditText etLoginTel;
    @BindView(R.id.et_loginPwd)
    EditText etLoginPwd;
    @BindView(R.id.btn_login)
    Button btnLogin;

    private SVProgressHUD svProgressHUD;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        unbinder = ButterKnife.bind(this);
        svProgressHUD = new SVProgressHUD(this);
        if (SharedPreferenceUtils.getContainsKey("uid")) {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            LoginActivity.this.startActivity(intent);
            LoginActivity.this.finish();
        }
    }

    @OnClick(R.id.btn_login)
    public void logo() {
        if (etLoginTel.getText().toString().equals("")) {
            svProgressHUD.showInfoWithStatus("请输入用户名/账号！");
        } else if (etLoginPwd.getText().toString().equals("")) {
            svProgressHUD.showInfoWithStatus("请输入密码");
        } else {
            svProgressHUD.showWithStatus("正在登录中...");
            new Thread() {
                @Override
                public void run() {
                    Call<String> call = OkGo.<String>get(NetWorkApi.getLoginApi)
                            .params("username", etLoginTel.getText().toString())
                            .params("password", etLoginPwd.getText().toString())
                            .converter(new StringConvert())
                            .adapt();
                    call.execute(new StringCallback() {
                        @Override
                        public void onSuccess(Response<String> response) {
                            UserInfo userInfo = new Gson().fromJson(response.body().toString(), UserInfo.class);
                            loginSuccess(userInfo);
                            Log.e("ok", userInfo.toString());
                        }

                        /**
                         *  服务器错误
                         * @param response
                         */
                        @Override
                        public void onError(Response<String> response) {
                            loginError(response);
                            super.onError(response);
                        }

                    });
                }
            }.start();
        }
    }

    private void loginError(Response<String> response) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                svProgressHUD.showErrorWithStatus("服务器错误！");
            }
        });
    }

    /**
     * 登录
     *
     * @param userInfo
     */
    private void loginSuccess(final UserInfo userInfo) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                svProgressHUD.dismissImmediately();
                /**
                 *  登录成功
                 */
                if (userInfo.getBaseResponse().getRet() == 0) {
                    SharedPreferenceUtils.setStringData("uid", userInfo.getUserList().getUid() + "");
                    SharedPreferenceUtils.setStringData("token", userInfo.getToken());
                    SharedPreferenceUtils.setStringData("car_no", userInfo.getUserList().getCar_no());
                    SharedPreferenceUtils.setStringData("username", userInfo.getUserList().getName());
//                    svProgressHUD.showSuccessWithStatus("登录成功");
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    LoginActivity.this.startActivity(intent);
                    LoginActivity.this.finish();
                } else {
                    svProgressHUD.showErrorWithStatus("账号或者密码错误!");
                }
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (unbinder != null)
            unbinder.unbind();
    }
}
